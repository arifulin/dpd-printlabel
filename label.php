<?php

$soap = new SoapClient('https://public-ws-stage.dpd.com/services/ShipmentService/V3_2/?wsdl');

$order = [
    //generalShipmentData
    'generalShipmentData' => [
        'identificationNumber' => '77777',
        'sendingDepot' => '0163',
        'product' => 'CL',
        'mpsCompleteDelivery' => 0,
        'sender' => [
            'name1' => 'Amnf',
            'street' => 'Strasse 1',
            'country' => 'DE',
            'zipCode' => '11111',
            'city' => 'Ort1',
            'customerNumber' => '12345679'
        ],
        'recipient' => [
            'name1' => 'Amnf',
            'street' => 'Strasse 1',
            'country' => 'DE',
            'zipCode' => '63741',
            'city' => 'Aschaffenburg',
            'state' => 'BY'
        ],
    ],
    'parcels' => [
        'parcelLabelNumber' => 12234562164],
    'productAndServiceData' => [
        'orderType' => 'consignment'
    ]
];

$params = [
    'delisId' => 'sandboxdpd',
    'password' => 'xMmshh1',
    'messageLanguage' => 'en_EN',
    'printOptions' => [
        'printerLanguage' => 'PDF',
        'paperFormat' => 'A4'
    ],
    'order' => $order
    ];

$obj = new StdClass;

try {
    //soap call
    $result = $soap->__soapCall('storeOrders', [$params]);



/*    if ($result->return->getParcellabelsPDF() != null) {
        String myBase64PDF = new String(Base64.encode(myStoreOrdersResponse.getParcellabelsPDF()));
							System.out.println("Base64 PDF String: " + myBase64PDF + "\n\n");
							byte[] pdf_bytes = myStoreOrdersResponse.getParcellabelsPDF();
							FileOutputStream fos = new FileOutputStream(
            "C://Users/.../Desktop/Test-PDF.pdf");
							fos.write(pdf_bytes);
							fos.close();
						}*/

} catch (SoapFault $e) {
    echo 'Caught exception: ', $e->getMessage(), "\n";
    $this->log($this->__getLastRequest());
}


var_dump($result);
//var_dump($obj);