<?php

$soap = new SoapClient('https://public-ws-stage.dpd.com/services/LoginService/V2_0/?wsdl');


$params = [
    'delisId' => 'sandboxdpd',
    'password' => 'xMmshh1',
    'messageLanguage' => 'en_EN'];

$obj = new StdClass;

try {
    //soap call
    $result = $soap->__soapCall('getAuth', [$params]);

    //var_dump($result);

    if ($result->return->authToken) {
        $obj->success = true;
        $obj->authToken = $result->return->authToken;
    } else $obj->success = false;

} catch (SoapFault $e) {
    echo 'Caught exception: ', $e->getMessage(), "\n";
    $this->log($this->__getLastRequest());
}
var_dump($obj);